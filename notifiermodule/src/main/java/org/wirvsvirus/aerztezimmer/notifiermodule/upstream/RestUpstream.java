package org.wirvsvirus.aerztezimmer.notifiermodule.upstream;

import java.net.URI;
import java.time.Clock;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpHeaders;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.Appointment;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.SmsCache;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.User;

@Service
public class RestUpstream {

	private static final String APPOINTMENTS_GET_APPOINTMENTS = "appointments/getAppointmentsWhichShouldBeNotified";

	private static final String SMS_CACHE_GET_BY_APPOINTMENT = "smsCaches/getSmsCacheByAppointmentId";

	private static final String POST_SMS_CACHE_OBJECT = "smsCaches/saveSmsCache";

	private static final String SEARCH_USER_BY_APPOINTMENT = "users/search/userByAppointmentId";
	private static String ROOT_URL;
	@Autowired
	private RestTemplate restTemplate;

	public RestUpstream(@Value("${spring.jpaurl}") String baseUrl) {
		ROOT_URL = "http://" + baseUrl + "/api/v1/";
	}

	public List<Appointment> getScheduledAppointments() {
		HttpHeaders headers = getBasicHeaders();
		UriComponents build = UriComponentsBuilder.fromHttpUrl(ROOT_URL + APPOINTMENTS_GET_APPOINTMENTS).build();
		ResponseEntity<List<Appointment>> response = restTemplate.exchange(
				RequestEntity.get(build.toUri()).headers(headers).build(),
				new ParameterizedTypeReference<List<Appointment>>() {
				});
		for (Appointment a : response.getBody()) {
			build = UriComponentsBuilder.fromHttpUrl(ROOT_URL + SMS_CACHE_GET_BY_APPOINTMENT)
					.queryParam("id", a.getId()).build();
			ResponseEntity<SmsCache> currentSmsCacheObject = restTemplate.exchange(
					RequestEntity.get(build.toUri()).headers(headers).build(),
					new ParameterizedTypeReference<SmsCache>() {
					});
			build = UriComponentsBuilder.fromHttpUrl(ROOT_URL + SEARCH_USER_BY_APPOINTMENT)
					.queryParam("appointmentId", a.getId()).build();
			ResponseEntity<User> userEntity = restTemplate.exchange(
					RequestEntity.get(build.toUri()).headers(getBasicHeaders()).build(),
					new ParameterizedTypeReference<User>() {
					});
			a.setUser(userEntity.getBody());
			a.setSmsCache(currentSmsCacheObject.getBody());
		}
		return response.getBody();
	}

	private HttpHeaders getBasicHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization",
				String.format("Basic " + Base64.getEncoder().encodeToString("patrick:123".getBytes())));
		return headers;
	}

	public void persistSmsCacheObject(AppointmentSmsCacheJsonPojo smsCache) {
		restTemplate.exchange(RequestEntity.post(URI.create(ROOT_URL + POST_SMS_CACHE_OBJECT))
				.headers(getBasicHeaders()).body(smsCache), String.class);
	}

	private String getLocalDateTime(Duration offset) {
		return LocalDateTime.now(Clock.offset(Clock.system(ZoneId.of("Europe/Berlin")), offset))
				.format(DateTimeFormatter.ISO_DATE_TIME);
	}
}
