package org.wirvsvirus.aerztezimmer.notifiermodule.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.notifiermodule.scheduler.SmsService;

@RestController
@RequestMapping("/api/v1/sms/")
public class SmsMessagingController {

	@Autowired
	private SmsService smsService;

	@PostMapping("/sendIncomeSms")
	public ResponseEntity sendIncomeSmsToUser(@RequestBody AppointmentSmsCacheJsonPojo appointment) {
		appointment.getAppointment().setSmsCache(appointment.getSmsCache());
		appointment.getAppointment().setUser(appointment.getUser());
		smsService.sendSmsMessageWithCaching(appointment.getAppointment(), "Sie sind nun dran!");
		return ResponseEntity.ok(null);
	}

}
