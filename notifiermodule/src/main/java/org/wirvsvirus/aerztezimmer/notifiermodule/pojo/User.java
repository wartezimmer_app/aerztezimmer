package org.wirvsvirus.aerztezimmer.notifiermodule.pojo;

import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class User {

	private Long id;

	private String username;

	private String password;

	private Set<Appointment> appointments;

	private String usernameShort;
}
