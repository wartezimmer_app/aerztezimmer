package org.wirvsvirus.aerztezimmer.notifiermodule;

import java.util.Map;
import java.util.function.Consumer;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentialsProvider;
import software.amazon.awssdk.awscore.AwsRequestOverrideConfiguration;
import software.amazon.awssdk.awscore.exception.AwsServiceException;
import software.amazon.awssdk.core.SdkField;
import software.amazon.awssdk.core.exception.SdkClientException;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.AuthorizationErrorException;
import software.amazon.awssdk.services.sns.model.EndpointDisabledException;
import software.amazon.awssdk.services.sns.model.InternalErrorException;
import software.amazon.awssdk.services.sns.model.InvalidParameterException;
import software.amazon.awssdk.services.sns.model.InvalidParameterValueException;
import software.amazon.awssdk.services.sns.model.InvalidSecurityException;
import software.amazon.awssdk.services.sns.model.KmsAccessDeniedException;
import software.amazon.awssdk.services.sns.model.KmsDisabledException;
import software.amazon.awssdk.services.sns.model.KmsInvalidStateException;
import software.amazon.awssdk.services.sns.model.KmsNotFoundException;
import software.amazon.awssdk.services.sns.model.KmsOptInRequiredException;
import software.amazon.awssdk.services.sns.model.KmsThrottlingException;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.NotFoundException;
import software.amazon.awssdk.services.sns.model.PlatformApplicationDisabledException;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.PublishResponse;
import software.amazon.awssdk.services.sns.model.SetSmsAttributesRequest;
import software.amazon.awssdk.services.sns.model.SetSmsAttributesResponse;
import software.amazon.awssdk.services.sns.model.SnsException;
import software.amazon.awssdk.services.sns.model.SnsRequest;
import software.amazon.awssdk.services.sns.model.ThrottledException;
import software.amazon.awssdk.services.sns.model.PublishRequest.Builder;

@SpringBootApplication
@EnableScheduling
public class SpringBootMain {
	@Autowired
	private ObjectMapper objectMapper;

	@Value("${spring.snsSecretAccessKey}")
	private String snsSecretAccesKey;

	@Value("${spring.snsAccessKeyId}")
	private String snsAccesKeyId;

	@Value("${spring.snsRegion}")
	private String region;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootMain.class, args);
	}

	@Profile("prod")
	@Bean
	public SnsClient getSmsProperties() {
		return SnsClient.builder().credentialsProvider(new AwsCredentialsProvider() {
			@Override
			public AwsCredentials resolveCredentials() {
				AwsCredentials credentials = new AwsCredentials() {

					@Override
					public String secretAccessKey() {
						return snsSecretAccesKey;
					}

					@Override
					public String accessKeyId() {
						return snsAccesKeyId;
					}
				};
				return credentials;
			}
		}).region(Region.of(this.region)).build();
	}

	@Profile("default")
	@Bean
	public SnsClient dummySnsClient() {
		return new SnsClient() {

			@Override
			public void close() {

			}

			@Override
			public String serviceName() {
				return null;
			}

			@Override
			public PublishResponse publish(Consumer<Builder> publishRequest)
					throws InvalidParameterException, InvalidParameterValueException, InternalErrorException,
					NotFoundException, EndpointDisabledException, PlatformApplicationDisabledException,
					AuthorizationErrorException, KmsDisabledException, KmsInvalidStateException, KmsNotFoundException,
					KmsOptInRequiredException, KmsThrottlingException, KmsAccessDeniedException,
					InvalidSecurityException, AwsServiceException, SdkClientException, SnsException {
				return PublishResponse.builder().build();
			}

			@Override
			public PublishResponse publish(PublishRequest publishRequest)
					throws InvalidParameterException, InvalidParameterValueException, InternalErrorException,
					NotFoundException, EndpointDisabledException, PlatformApplicationDisabledException,
					AuthorizationErrorException, KmsDisabledException, KmsInvalidStateException, KmsNotFoundException,
					KmsOptInRequiredException, KmsThrottlingException, KmsAccessDeniedException,
					InvalidSecurityException, AwsServiceException, SdkClientException, SnsException {
				return PublishResponse.builder().build();
			}

			@Override
			public SetSmsAttributesResponse setSMSAttributes(
					Consumer<software.amazon.awssdk.services.sns.model.SetSmsAttributesRequest.Builder> setSmsAttributesRequest)
					throws InvalidParameterException, ThrottledException, InternalErrorException,
					AuthorizationErrorException, AwsServiceException, SdkClientException, SnsException {
				return null;
			}

			@Override
			public SetSmsAttributesResponse setSMSAttributes(SetSmsAttributesRequest setSmsAttributesRequest)
					throws InvalidParameterException, ThrottledException, InternalErrorException,
					AuthorizationErrorException, AwsServiceException, SdkClientException, SnsException {
				return null;
			}
		};
	}

	@PostConstruct
	public void setUp() {
		this.objectMapper.findAndRegisterModules();
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
