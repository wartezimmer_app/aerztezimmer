package org.wirvsvirus.aerztezimmer.notifiermodule.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.Appointment;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.SmsCache;
import org.wirvsvirus.aerztezimmer.notifiermodule.upstream.RestUpstream;

import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.MessageAttributeValue;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.SetSmsAttributesRequest;

@Service
@Slf4j
public class SmsSchedulerService {

	@Autowired
	private RestUpstream restUpstream;

	@Autowired
	private SmsService smsService;

	@Scheduled(fixedDelay = 10000)
	public void sendSms() {
		List<Appointment> scheduledAppointments = restUpstream.getScheduledAppointments();
		smsService.sendSmsMessagesWithCachingForList(scheduledAppointments);
	}

}
