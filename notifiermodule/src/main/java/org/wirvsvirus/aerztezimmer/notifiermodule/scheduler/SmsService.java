package org.wirvsvirus.aerztezimmer.notifiermodule.scheduler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.Appointment;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.notifiermodule.pojo.SmsCache;
import org.wirvsvirus.aerztezimmer.notifiermodule.upstream.RestUpstream;

import lombok.extern.slf4j.Slf4j;
import software.amazon.awssdk.services.sns.SnsClient;
import software.amazon.awssdk.services.sns.model.PublishRequest;
import software.amazon.awssdk.services.sns.model.SetSmsAttributesRequest;

@Service
@Slf4j
public class SmsService {
	@Autowired
	private SnsClient snsClient;

	@Autowired
	private RestUpstream restUpstream;

	public void sendSmsMessagesWithCachingForList(List<Appointment> scheduledAppointments) {
		for (Appointment a : scheduledAppointments) {
			String message = "Sie erhalten hiermit Ihre Benachrichtigung zum Arzttermin, der in "
					+ a.getPatientWayTime()
					+ " Minuten angesetzt ist. Bitte bestätigen oder widerrufen Sie ihn unter diesem Link: "
					+ "https://warte.cloud/#/validate/" + a.getUuid() + ". Vielen Dank!";
			if (!a.isValidated()) {
				sendSmsMessageWithCaching(a, message);
			}
		}
	}

	public void sendSmsMessageWithCaching(Appointment a, String message) {
		if (a.getSmsCache() == null) {
			sendSmsMessageImpl(message, a);
			restUpstream
					.persistSmsCacheObject(new AppointmentSmsCacheJsonPojo(new SmsCache(LocalDateTime.now(), a), a));
		} else {
			if (LocalDateTime.now().minus(Duration.ofMinutes(5)).isAfter(a.getSmsCache().getLastSended())) {
				a.getSmsCache().setLastSended(LocalDateTime.now());
				restUpstream.persistSmsCacheObject(
						new AppointmentSmsCacheJsonPojo(new SmsCache(LocalDateTime.now(), a), a));
				sendSmsMessageImpl(message, a);
			}
		}
	}

	private void sendSmsMessageImpl(String message, Appointment appointment) {
		snsClient.setSMSAttributes(SetSmsAttributesRequest.builder().attributes(
				Map.of("DefaultSMSType", "Transactional", "DefaultSenderID", appointment.getUser().getUsernameShort()))
				.build());
		log.debug("Sending message " + message);
		snsClient.publish(PublishRequest.builder()
				.subject("Arztbesuch am " + appointment.getDateTime().format(DateTimeFormatter.ISO_DATE))
				.message(message).phoneNumber(appointment.getTelephone()).build());
	}
}
