package org.wirvsvirus.aerztezimmer.notifiermodule.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentSmsCacheJsonPojo {

	private SmsCache smsCache;

	private Appointment appointment;

	private User user;

	public AppointmentSmsCacheJsonPojo(SmsCache smsCache, Appointment appointment) {
		this.smsCache = smsCache;
		this.appointment = appointment;
	}
}
