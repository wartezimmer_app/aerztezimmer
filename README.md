# Project-Brief
- Patienten in Wartezimmern sind größter "Spreader"von Krankheiten, damit werden wartende durch evtl. infizierte Patienten krank
- Anzahl der Menschen im Wartezimmer reduzieren durch:
  - nach Terminvereinbarung in Praxis legt authentifizierte Praxis einen Termin mit dem Patienten und dessen Handy/E-Mail an
  - Patient erhält benachritigung mit eindeutiger URL zu dem Termin / QR Code ggf bei ad-hoc Termin
  - 1h vor Termin erhält Patient einen reminder mit Anforderung Check IN (Wird Termin wahrgenommen ja/nein)
  - optional: Über Verkehrslage empfehlung zur Abfahrt
  - Patient wartet im Auto vor der Praxis
  - Patient wird durch SMS/Push Nachricht von Praxis aufgerufen

API-Möglichkeit zur Einbindung in Arzt-Systeme

# Build
Um das Projekt zu bauen, bitte folgende Steps durchführen:
1. mvn clean install im root folder des Projekts ausführen.
2. Im jpamodule-folder den command mvn clean package spring-boot:run ausführen. Hierbei bitte darauf achten, dass lokal eine MariaDB-Datenbank läuft. Sollte keine lokale Instanz laufend sein,
   kann auch mit den Befehlen mvn clean package spring-boot:repackage, docker-compose rm docker-compose build und docker-compose up ein entsprechendes Docker-Deployment ausgeführt werden, in dem
   dann die Spring-Boot App zusammen mit einer MariaDB läuft.
3. Ein anderes Terminal öffnen, um im notifiermodule-folder den command mvn clean package spring-boot:run ausführen.
4. Fertig :)

# Deploy
Um das Projekt zu deployen, sind folgende Schritte notwendig:
1. mvn clean install im root folder des Projekts ausführen.
2. Im jpamodule-folder sowie dem notifiermodule-folder den command mvn clean package spring-boot:repackage ausführen. Nun kann man entweder die produzierten Jars im Tar-Folder starten, oder
   man baut mit docker build . -f Dockerfile-jpamodule repektive docker build . docker container, die dann in die Cloud deployed werden können. Entsprechendes Tagging ist hier natürlich pflicht!
3. Fertig :)


# Team
Nico: Backend Dev (Java, Springboot)
Sascha: Backend Dev / Fullstack (Java, Springboot)
Marco: Frontend (Angular/Bootstrap) + Strategie PM
Patrick: Backend Dev (Java/Springboot)
Thiemo: Backend Dev (Java Springboot)
Jan: Frontend/UX?
Tobias: Cloud/PM/Frontend
Johannes: Quality-Gate