create database warten;
CREATE USER 'warten'@'%' IDENTIFIED BY 'wartenpwd';
FLUSH PRIVILEGES;
grant all on *.* to 'warten'@'%' IDENTIFIED BY 'wartenpwd';
FLUSH PRIVILEGES;
