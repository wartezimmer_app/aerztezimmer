package org.wirvsvirus.aerztezimmer;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.wirvsvirus.aerztezimmer.controller.AppointmentControllerImpl;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.User;
import org.wirvsvirus.aerztezimmer.repositories.AppointmentRepository;
import org.wirvsvirus.aerztezimmer.repositories.UserRepository;

@SpringBootTest
@ContextConfiguration(classes = AerztezimmerApplication.class)
@TestInstance(Lifecycle.PER_CLASS)
public class AppointmentControllerImplTest {

	@Autowired
	private AppointmentControllerImpl appointmentControllerImpl;

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private AppointmentRepository appointmentRepo;

	@BeforeAll
	public void before() {
		User user = new User();
		user.setUsername("test");
		user.setPassword("test");
		user.setUsernameShort("test");
		userRepository.save(user);
		Appointment appointment = new Appointment();
		appointment.setDateTime(LocalDateTime.now().plusMinutes(39));
		appointment.setName("termin");
		appointment.setPatientWayTime(40);
		appointment.setUser(user);
		appointmentRepo.save(appointment);
		Appointment appointmentTwo = new Appointment();
		appointmentTwo.setDateTime(LocalDateTime.now().plusMinutes(30));
		appointmentTwo.setPatientWayTime(28);
		appointmentTwo.setUser(user);
		appointmentRepo.save(appointmentTwo);
	}

	@AfterAll
	public void after() {
		appointmentRepo.deleteAll();
		userRepository.deleteAll();
	}

	@Test
	public void testGetAppointmentsBetweenDate() {
		ResponseEntity<List<Appointment>> appointmentsWhichShouldBeNotified = appointmentControllerImpl
				.getAppointmentsWhichShouldBeNotified();
		List<Appointment> body = appointmentsWhichShouldBeNotified.getBody();
		assertEquals(1, body.size());
		assertEquals(body.stream().findFirst().get().getPatientWayTime(), 40);
	}

}
