package org.wirvsvirus.aerztezimmer.entities;

import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "smscache")
@Getter
@Setter
@NoArgsConstructor
public class SmsCache {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "lastSended")
    @DateTimeFormat(iso = ISO.DATE_TIME)
    private LocalDateTime lastSended;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Appointment appointment;

    private boolean linkValidated;
}
