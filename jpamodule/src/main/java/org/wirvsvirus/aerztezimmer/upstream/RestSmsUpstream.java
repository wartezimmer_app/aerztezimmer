package org.wirvsvirus.aerztezimmer.upstream;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.wirvsvirus.aerztezimmer.entities.AppointmentSmsCacheJsonPojo;

@Service
public class RestSmsUpstream {

	private static String ROOT_URL;

	private static final String SMS_TRIGGER_URL = "sendIncomeSms";

	public RestSmsUpstream(@Value("${spring.notifierurl}") String baseUrl) {
		System.out.println(baseUrl);
		ROOT_URL = baseUrl + "/api/v1/sms/";
	}

	@Autowired
	private RestTemplate restTemplate;

	public void triggerSms(AppointmentSmsCacheJsonPojo mergedPojo) {
		ResponseEntity<Void> responseEntity = restTemplate
				.exchange(RequestEntity.post(URI.create(ROOT_URL + SMS_TRIGGER_URL)).body(mergedPojo), Void.class);
	}
}
