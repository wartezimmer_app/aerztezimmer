package org.wirvsvirus.aerztezimmer.controller.postbody.load;

import lombok.Data;

@Data
public class LoadAppointmentByUuidPostbody {
    String uuid;
}
