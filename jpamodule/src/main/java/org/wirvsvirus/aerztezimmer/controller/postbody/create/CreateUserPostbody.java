package org.wirvsvirus.aerztezimmer.controller.postbody.create;

import lombok.Data;

@Data
public class CreateUserPostbody {

    String username;
    String password;

}
