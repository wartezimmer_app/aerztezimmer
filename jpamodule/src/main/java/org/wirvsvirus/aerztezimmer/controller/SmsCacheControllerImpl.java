package org.wirvsvirus.aerztezimmer.controller;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.entities.SmsCache;
import org.wirvsvirus.aerztezimmer.repositories.AppointmentRepository;
import org.wirvsvirus.aerztezimmer.repositories.SmsCacheRepo;

@Controller
@RequestMapping(value = "/api/v1/smsCaches/")
public class SmsCacheControllerImpl {

	@Autowired
	private SmsCacheRepo smsCacheRepo;

	@Autowired
	private AppointmentRepository appointmentRepo;

	@GetMapping(value = "/getSmsCacheByAppointmentId")
	public ResponseEntity<SmsCache> getSmsCacheByAppointmentId(long id) {
		Optional<SmsCache> byAppointment = smsCacheRepo.findByAppointment(appointmentRepo.findById(id).get());
		if (byAppointment.isPresent()) {
			return ResponseEntity.ok(byAppointment.get());
		} else {
			return ResponseEntity.ok(null);
		}
	}

	@PostMapping(value = "/saveSmsCache")
	@ResponseBody
	@Transactional
	public ResponseEntity saveSmsCache(@RequestBody AppointmentSmsCacheJsonPojo smsCache) {
		System.out.println(smsCache.getAppointment().getId());
		Appointment appointment = appointmentRepo.findById(smsCache.getAppointment().getId()).get();
		if (appointment.getSmsCache() == null) {
			appointment.setSmsCache(new SmsCache());
			appointment.getSmsCache().setAppointment(appointment);
		}
		appointment.getSmsCache().setLastSended(smsCache.getSmsCache().getLastSended());
		appointmentRepo.save(appointment);
		return ResponseEntity.ok().build();
	}
}
