package org.wirvsvirus.aerztezimmer.controller.postbody.update;

import lombok.Data;

@Data
public class DelayAppointmentsPostbody {
    private int minutes;
    private String dateTime;
}
