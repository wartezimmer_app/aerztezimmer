package org.wirvsvirus.aerztezimmer.controller.postbody.update;

import lombok.AllArgsConstructor;

import lombok.NoArgsConstructor;

import lombok.Setter;

import lombok.Getter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ValidationPostbody {

    private boolean valid;

    private String uuid;
}
