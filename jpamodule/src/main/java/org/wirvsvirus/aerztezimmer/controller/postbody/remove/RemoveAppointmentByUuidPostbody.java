package org.wirvsvirus.aerztezimmer.controller.postbody.remove;

import lombok.Data;

@Data
public class RemoveAppointmentByUuidPostbody {
    String uuid;
}
