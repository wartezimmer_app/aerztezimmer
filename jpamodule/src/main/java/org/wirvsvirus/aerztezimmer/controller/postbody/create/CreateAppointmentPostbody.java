package org.wirvsvirus.aerztezimmer.controller.postbody.create;

import lombok.Data;

@Data
public class CreateAppointmentPostbody {

    private String name;
    private String telephone;
    private String dateTime;
    private String waytime;
}
