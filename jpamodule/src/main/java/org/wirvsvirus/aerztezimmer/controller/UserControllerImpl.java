package org.wirvsvirus.aerztezimmer.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.wirvsvirus.aerztezimmer.controller.postbody.create.CreateUserPostbody;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.User;
import org.wirvsvirus.aerztezimmer.repositories.AppointmentRepository;
import org.wirvsvirus.aerztezimmer.repositories.UserRepository;
import org.wirvsvirus.aerztezimmer.utils.AuthHeaderHelper;

@Api
@RestController
@RequestMapping(value = "/api/v1/users/")
@CrossOrigin
public class UserControllerImpl {

	private final UserRepository userRepository;
	private final PasswordEncoder passwordEncoder;

	@Autowired
	private AppointmentRepository appointmentRepo;

	@Autowired
	public UserControllerImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@ApiOperation("Checks if user is existant in database")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	ResponseEntity<User> login(@RequestHeader("Authorization") String authHeader) {
		final User userFromDb = userRepository.findByUsername(AuthHeaderHelper.extractAuthHeader(authHeader));
		if (userFromDb != null) {
			return ResponseEntity.ok(userFromDb);
		} else {
			return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
		}
	}

	@ApiOperation("Registers a new user")
	@RequestMapping(value = "/create", method = RequestMethod.POST)
	ResponseEntity<User> createUser(@RequestBody CreateUserPostbody createUserPostbody) {
		final User userToCreate = new User();
		userToCreate.setUsername(createUserPostbody.getUsername());
		userToCreate.setPassword(passwordEncoder.encode(createUserPostbody.getPassword()));
		return ResponseEntity.ok(userRepository.save(userToCreate));
	}

	@RequestMapping(value = "/search/userByAppointmentId", method = RequestMethod.GET)
	@ApiOperation("Gets a user by an specific appointment id")
	ResponseEntity<User> searchUserByAppointmentId(@RequestParam Long appointmentId) {
		Optional<Appointment> appointmentByIdOptional = appointmentRepo.findById(appointmentId);
		if (!appointmentByIdOptional.isPresent()) {
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.ok(appointmentByIdOptional.get().getUser());
	}
}
