package org.wirvsvirus.aerztezimmer.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.wirvsvirus.aerztezimmer.controller.postbody.create.CreateAppointmentPostbody;
import org.wirvsvirus.aerztezimmer.controller.postbody.load.LoadAppointmentByUuidPostbody;
import org.wirvsvirus.aerztezimmer.controller.postbody.remove.RemoveAppointmentByUuidPostbody;
import org.wirvsvirus.aerztezimmer.controller.postbody.update.DelayAppointmentsPostbody;
import org.wirvsvirus.aerztezimmer.controller.postbody.update.ValidationPostbody;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.AppointmentSmsCacheJsonPojo;
import org.wirvsvirus.aerztezimmer.entities.SmsCache;
import org.wirvsvirus.aerztezimmer.entities.User;
import org.wirvsvirus.aerztezimmer.repositories.AppointmentRepository;
import org.wirvsvirus.aerztezimmer.repositories.SmsCacheRepo;
import org.wirvsvirus.aerztezimmer.repositories.UserRepository;
import org.wirvsvirus.aerztezimmer.upstream.RestSmsUpstream;
import org.wirvsvirus.aerztezimmer.utils.AppointmentUtils;
import org.wirvsvirus.aerztezimmer.utils.AuthHeaderHelper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/v1/appointments/")
@CrossOrigin()
public class AppointmentControllerImpl {

	private final UserRepository userRepository;
	private final AppointmentRepository appointmentRepository;

	@Autowired
	private AppointmentRepository appointmentRepo;

	@Autowired
	private SmsCacheRepo smsCacheRepo;

	@Autowired
	private RestSmsUpstream restSmsUpstream;

	@Autowired
	public AppointmentControllerImpl(UserRepository userRepository, AppointmentRepository appointmentRepository) {
		this.userRepository = userRepository;
		this.appointmentRepository = appointmentRepository;
	}

	@ApiOperation("creates an appointment for the given user")
	@RequestMapping(value = "/create", method = RequestMethod.PUT)
	ResponseEntity<Appointment> createAppointment(@RequestBody CreateAppointmentPostbody createAppointmentPostbody,
			@RequestHeader("Authorization") String authHeader) {
		final Appointment appointment = new Appointment();
		if (createAppointmentPostbody.getWaytime() != null && !createAppointmentPostbody.getWaytime().isEmpty()) {
			appointment.setPatientWayTime(Integer.valueOf(createAppointmentPostbody.getWaytime()));
		}
		appointment.setUuid(UUID.randomUUID().toString());
		appointment.setUser(userRepository.findByUsername(AuthHeaderHelper.extractAuthHeader(authHeader)));
		appointment.setName(createAppointmentPostbody.getName());

		appointment.setDateTime(
				LocalDateTime.from(DateTimeFormatter.ISO_DATE_TIME.parse(createAppointmentPostbody.getDateTime())));
		appointment.setTelephone(AppointmentUtils.normalizeTelephoneNumber(createAppointmentPostbody.getTelephone()));

		return ResponseEntity.ok(appointmentRepository.save(appointment));
	}

	@GetMapping(value = "/getAppointmentsWhichShouldBeNotified")
	public ResponseEntity<List<Appointment>> getAppointmentsWhichShouldBeNotified() {
		Iterable<Appointment> allAppointments = appointmentRepo.findAll();
		List<Appointment> appointmentsToReturn = new ArrayList<>();
		for (Appointment a : allAppointments) {
			if (a.getDateTime().minusMinutes(a.getPatientWayTime()).isBefore(LocalDateTime.now()) && !a.isValidated()) {
				appointmentsToReturn.add(a);
			}
		}
		return ResponseEntity.ok(appointmentsToReturn);
	}

	@ApiOperation("searches all of appointments of a user")
	@RequestMapping(value = "/search/personal", method = RequestMethod.GET)
	ResponseEntity<List<Appointment>> getMyAppointments(@RequestHeader("Authorization") String authHeader) {
		final User callingUser = userRepository.findByUsername(AuthHeaderHelper.extractAuthHeader(authHeader));
		return ResponseEntity.ok(appointmentRepository.findAllByUser(callingUser));
	}

	@ApiOperation("searches all of appointments of a user")
	@RequestMapping(value = "/search/today", method = RequestMethod.GET)
	ResponseEntity<List<Appointment>> getTodaysAppointments(@RequestHeader("Authorization") String authHeader) {
		final User callingUser = userRepository.findByUsername(AuthHeaderHelper.extractAuthHeader(authHeader));
		final LocalDateTime startOfDay = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
		final LocalDateTime endOfDay = LocalDateTime.of(LocalDate.now(), LocalTime.of(23, 59));
		return ResponseEntity.ok(appointmentRepository.findAllByUserAndDateTimeBetweenOrderByDateTimeAsc(callingUser,
				startOfDay, endOfDay));
	}

	@ApiOperation("updates an appointment after user validation via sms")
	@RequestMapping(value = "/update/appointmentValidation", method = RequestMethod.POST)
	public ResponseEntity validateAppointment(@RequestBody ValidationPostbody validationPostbody) {
		Optional<Appointment> appointmentByUuid = appointmentRepository.findOneByUuid(validationPostbody.getUuid());
		if (!appointmentByUuid.isPresent()) {
			return ResponseEntity.badRequest().body(null);
		}
		Appointment appointment = appointmentByUuid.get();
		if (validationPostbody.isValid()) {
			appointment.setValidated(true);
			appointmentRepository.save(appointment);
		} else {
			appointmentRepository.delete(appointment);
		}
		return ResponseEntity.ok(null);
	}

	@ApiOperation("load an appointment by a give id")
	@RequestMapping(value = "/load/byUuid", method = RequestMethod.POST)
	ResponseEntity<Appointment> getAppointmentByUuid(
			@RequestBody LoadAppointmentByUuidPostbody loadAppointmentByUuidPostbody) {
		final Optional<Appointment> possibleAppointmentUuid = appointmentRepository
				.findOneByUuid(loadAppointmentByUuidPostbody.getUuid());
		return possibleAppointmentUuid.map(ResponseEntity::ok).orElseGet(() -> ResponseEntity.notFound().build());
	}

	@ApiOperation("Triggers a sms to a users handy which says that he/she can come in")
	@RequestMapping(value = "/triggerSms", method = RequestMethod.POST)
	public ResponseEntity triggerSmsToUser(@RequestBody LoadAppointmentByUuidPostbody loadAppointmentByUuidPostbody) {
		Optional<Appointment> appointmentByUuidOptional = appointmentRepo
				.findOneByUuid(loadAppointmentByUuidPostbody.getUuid());
		if (!appointmentByUuidOptional.isPresent()) {
			return ResponseEntity.noContent().build();
		}
		Appointment appointmentByUuid = appointmentByUuidOptional.get();
		SmsCache smsCache = appointmentByUuid.getSmsCache();
		if (smsCache == null) {
			return ResponseEntity.noContent().build();
		}
		AppointmentSmsCacheJsonPojo mergedPojo = new AppointmentSmsCacheJsonPojo(smsCache, appointmentByUuid,
				appointmentByUuid.getUser());
		restSmsUpstream.triggerSms(mergedPojo);
		return ResponseEntity.ok().build();
	}

	@ApiOperation("Delays all appointments of a user by a given number of minutes after a given date")
	@RequestMapping(value = "/update/delayAfterAppointment", method = RequestMethod.PATCH)
	ResponseEntity<Appointment> delayAppointmentsAfterAppointment(
			@RequestBody(required = false) DelayAppointmentsPostbody delayAppointmentsPostbody,
			@RequestHeader("Authorization") String authHeader) {
		final User callingUser = userRepository.findByUsername(AuthHeaderHelper.extractAuthHeader(authHeader));
		LocalDateTime timeAtWhichToDelay;
		if (delayAppointmentsPostbody != null && delayAppointmentsPostbody.getDateTime() != null) {
			timeAtWhichToDelay = LocalDateTime
					.from(DateTimeFormatter.ISO_DATE_TIME.parse(delayAppointmentsPostbody.getDateTime()));
		} else {
			timeAtWhichToDelay = LocalDateTime.now();
		}
		final List<Appointment> allUserAppointments = appointmentRepository.findAllByUserAndDateTimeAfter(callingUser,
				timeAtWhichToDelay);

		for (Appointment singleAppointment : allUserAppointments) {
			singleAppointment
					.setDateTime(singleAppointment.getDateTime().plusMinutes(delayAppointmentsPostbody.getMinutes()));
		}

		appointmentRepository.saveAll(allUserAppointments);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation("Removen an appointment by a given uuid")
	@RequestMapping(value = "/remove/byUuid", method = RequestMethod.PATCH)
	ResponseEntity<Object> removeAppointment(
			@RequestBody RemoveAppointmentByUuidPostbody removeAppointmentByUuidPostbody) {
		final Optional<Appointment> possibleAppointmentUuid = appointmentRepository
				.findOneByUuid(removeAppointmentByUuidPostbody.getUuid());
		if (possibleAppointmentUuid.isPresent()) {
			appointmentRepository.delete(possibleAppointmentUuid.get());
			return ResponseEntity.noContent().build();
		}
		return ResponseEntity.notFound().build();
	}
}
