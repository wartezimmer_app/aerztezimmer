package org.wirvsvirus.aerztezimmer.repositories;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.SmsCache;

@Repository
public interface SmsCacheRepo extends PagingAndSortingRepository<SmsCache, Long> {

    
    public Optional<SmsCache> findByAppointment(Appointment appointment);
}
