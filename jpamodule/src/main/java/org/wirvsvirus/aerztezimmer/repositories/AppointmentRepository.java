package org.wirvsvirus.aerztezimmer.repositories;

import io.swagger.annotations.Api;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Repository;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Api(tags = "AppointmentRepo")
@Repository()
public interface AppointmentRepository extends PagingAndSortingRepository<Appointment, Long> {

	List<Appointment> findAllByUserAndDateTimeAfter(User user,
			@DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime dateTime);

	List<Appointment> findAllByUser(User user);

	Optional<Appointment> findOneByUuid(String uuid);

	List<Appointment> findByDateTimeBetween(@DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime startTime,
			@DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime endTime);

	List<Appointment> findAllByUserAndDateTimeBetweenOrderByDateTimeAsc(User user,
			@DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime startTime,
			@DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime endTime);

}
