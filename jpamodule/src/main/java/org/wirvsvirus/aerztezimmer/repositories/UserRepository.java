package org.wirvsvirus.aerztezimmer.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;
import org.wirvsvirus.aerztezimmer.entities.Appointment;
import org.wirvsvirus.aerztezimmer.entities.User;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.wirvsvirus.aerztezimmer.entities.User;

@Api(tags = "UserRepo")
@Repository

public interface UserRepository extends PagingAndSortingRepository<User, Long> {

	User findByUsername(String username);

}
