package org.wirvsvirus.aerztezimmer.utils;

import java.util.Base64;

public class AuthHeaderHelper {

    /**
     * Extracting user from base 64 string
     *
     * @param encryptedHeader raw header
     * @return username
     */
    public static String extractAuthHeader(String encryptedHeader) {
        encryptedHeader = encryptedHeader.replace("Basic ", "");
        return new String(Base64.getDecoder().decode(encryptedHeader.getBytes())).split(":")[0];
    }
}
