package org.wirvsvirus.aerztezimmer.utils;

public class AppointmentUtils {

    public static String normalizeTelephoneNumber(String number) {
        return number.replaceAll("[\\s-/]+", "").replaceAll("^0", "+49");
    }
}
