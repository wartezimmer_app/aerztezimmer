package org.wirvsvirus.aerztezimmer.startup;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.wirvsvirus.aerztezimmer.entities.User;
import org.wirvsvirus.aerztezimmer.repositories.UserRepository;

@Component
public class DefaultDataCreator implements ApplicationListener<ApplicationReadyEvent> {

	public static final Logger LOG = LoggerFactory.getLogger(DefaultDataCreator.class);

	private static final String ENV_PROD = "prod";
	private static final String DEFAULT_USER = "praxis1";
	private static final String DEFAULT_PASSWORD = "test";

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final Environment environment;

    @Autowired
    public DefaultDataCreator(UserRepository userRepository, PasswordEncoder passwordEncoder, Environment environment) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.environment = environment;
    }
		
    public void onApplicationEvent(ApplicationReadyEvent event) {
    	
		boolean isProd = Arrays.asList(environment.getActiveProfiles()).contains(ENV_PROD);
		if(isProd) {
			LOG.debug("Not creating user on PROD");
			return;
		} else {
    		
    		User defaultUser = userRepository.findByUsername(DEFAULT_USER);
    		if(defaultUser!=null) {
    			LOG.info("User {} already exists on", DEFAULT_USER);
    		} else {
    			final User userToCreate = new User();
    			userToCreate.setUsername(DEFAULT_USER);
    			userToCreate.setPassword(passwordEncoder.encode(DEFAULT_PASSWORD));
    			userRepository.save(userToCreate);  
    			LOG.info("Created test user {} for non-prod environment", DEFAULT_USER);
    		}
		
		}
    }

    
}
