package org.wirvsvirus.aerztezimmer.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AppoinmentUtilsUnitTest {

    @Test
    void test_telephoneNumber_normalization_plus_and_minus() {
        String rawNumber = "0234 910-98115";
        String normalizedNumber = AppointmentUtils.normalizeTelephoneNumber(rawNumber);
        assertEquals("+4923491098115", normalizedNumber);
    }

    @Test
    void test_telephoneNumber_normalization_multiple_spaces() {
        String rawNumber = "0234 910 98115";
        String normalizedNumber = AppointmentUtils.normalizeTelephoneNumber(rawNumber);
        assertEquals("+4923491098115", normalizedNumber);
    }

    @Test
    void test_telephoneNumber_normalization_slash() {
        String rawNumber = "0234/910 98115";
        String normalizedNumber = AppointmentUtils.normalizeTelephoneNumber(rawNumber);
        assertEquals("+4923491098115", normalizedNumber);
    }

    @Test
    void test_telephoneNumber_normalization_dutch() {
        String rawNumber = "+31 12 345 6789";
        String normalizedNumber = AppointmentUtils.normalizeTelephoneNumber(rawNumber);
        assertEquals("", "+31123456789", normalizedNumber);
    }

    @Test
    void test_telephoneNumber_normalization_dutch_2() {
        String rawNumber = "+31 12 345 67890";
        String normalizedNumber = AppointmentUtils.normalizeTelephoneNumber(rawNumber);
        assertEquals("", "+311234567890", normalizedNumber);
    }
}
