package org.wirvsvirus.aerztezimmer.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthHeaderHelperUnitTest {

    @Test
    void test_extract_user_from_auth_header() {
        final String username = AuthHeaderHelper.extractAuthHeader("Basic cGF0cmljazoxMjM=");
        assertEquals("patrick", username);
    }
}
